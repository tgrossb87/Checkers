#include "Board.h"
#include "Piece.h"
#include <iostream>
#include <string>
#include <tuple>

void move(Board*);
void set(Board*);
void get(Board*);
int convertInRange(const std::string &, int, int);
void test();

int main(){
	Board b;
	while (true){
		b.print();
		std::cout << "Enter a command - move (m), set (s), get (g), exit (e):  ";
		std::string cmd;
		std::cin >> cmd;
		if (cmd == "m" || cmd == "mo" || cmd == "mov" || cmd == "move")
			move(&b);
		if (cmd == "s" || cmd == "se" || cmd == "set")
			set(&b);
		if (cmd == "g" || cmd == "ge" || cmd == "get")
			get(&b);
		if (cmd == "e" || cmd == "ex" || cmd == "exi" || cmd == "exit")
			return 0;
	}

}

void move(Board *board){
	std::string ris, cis, rfs, cfs;
	int ri = -1, ci = -1, rf = -1, cf = -1;
	while (ri < 0){
		std::cout << "From row (0-7):  ";
		std::cin >> ris;
		ri = convertInRange(ris, 0, 7);
	}

	while (ci < 0){
		std::cout << "From column (0-7):  ";
		std::cin >> cis;
		ci = convertInRange(cis, 0, 7);
	}

	while (rf < 0){
		std::cout << "To row (0-7):  ";
		std::cin >> rfs;
		rf = convertInRange(rfs, 0, 7);
	}

	while (cf < 0){
		std::cout << "To col (0-7):  ";
		std::cin >> cfs;
		cf = convertInRange(cfs, 0, 7);
	}

	bool success = board->move({ri, ci}, {rf, cf});
	if (success)
		std::cout << "Successful!\n";
	else
		std::cout << "Not successful :(\n";
}

void set(Board *board){
	std::string ris, cis, ps;
	int ri = -1, ci = -1, piece = -1;
	while (ri < 0){
		std::cout << "Row (0-7):  ";
		std::cin >> ris;
		ri = convertInRange(ris, 0, 7);
	}

	while (ci < 0){
		std::cout << "Col (0-7):  ";
		std::cin >> cis;
		ci = convertInRange(cis, 0, 7);
	}

	while (piece < 0){
		std::cout << "Piece - Green (0), Green King (1), Red (2), Red King (3):  ";
		std::cin >> ps;
		piece = convertInRange(ps, 0, 3);
	}

	board->set({ri, ci}, Piece::typeFromIndex(piece));
}

void get(Board *board){
	std::string ris, cis;
	int ri = -1, ci = -1;
	while (ri < 0){
		std::cout << "Row (0-7):  ";
		std::cin >> ris;
		ri = convertInRange(ris, 0, 7);
	}

	while (ci < 0){
		std::cout << "Col (0-7):  ";
		std::cin >> cis;
		ci = convertInRange(cis, 0, 7);
	}

	std::cout << "Piece at [" << ri << ", " << ci << "] is " << *(board->getPiece({ri, ci})) << "\n";
}

int convertInRange(const std::string &str, int min, int max){
	if (str.length() == 0)
		return -1;
	std::string::size_type sz;
	int conv;

	try {
		conv = std::stoi(str, &sz);
	} catch (const std::invalid_argument&) {
		std::cout << str << "? come on, do it again, but better\n";
		return -1;
	}

	if (sz == str.length()-1){
		std::cout << str << "? really?\n";
		return -1;
	}

	if (conv > max || conv < min){
		std::cout << str << "? can you read? do you know what a range is?\n";
		return -1;
	}
	return conv;
}

void test(){
	Board b;
	b.set({0, 0}, Piece::GREEN);
	b.set({2, 5}, Piece::RED);

	b.print();

	if (!b.move({2, 5}, {3, 6}))
		std::cout << "Moving failed :_(\n";
	b.print();
}
