## Checkers ##
Here's some checkers written in C++.

## The idea ##
- main: Handles user input and translates it to board operations, restricts input to the proper user
- Board: Holds the state of the checkers board as a vector of piece pointers, allows for piece movement
- Piece: A simple representation of a piece in a few different ways

## Todo ##
[ ] Write the `validMove(std::tuple<int, std::string, std::string>, std::tuple<int, std::string, std::string>)` method to determine if a move is okay (**this is big**)
[-] Write the terminal interface for user input
[ ] Write the win and kinging logic
