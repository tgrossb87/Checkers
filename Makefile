TARGET = checkers

all: $(TARGET)

$(TARGET): main.cpp Board.cpp Piece.cpp
	g++ -std=c++17 -o $(TARGET) main.cpp Board.cpp Piece.cpp
	./$(TARGET)

main.cpp: Board.cpp Board.h Piece.cpp Piece.h

Board.cpp: Board.h Piece.cpp Piece.h

Board.h: Piece.h

Piece.cpp: Piece.h
