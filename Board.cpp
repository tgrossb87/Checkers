#include "Board.h"
#include "Piece.h"
#include <iostream>
#include <vector>
#include <string>
#include <tuple>
#include <boost/algorithm/string.hpp>

Board::Board(void){
	rows = 8;
	cols = 8;
//	board = std::vector<std::vector<Piece*>>(rows, std::vector<Piece*>(cols, new Piece(Piece::EMPTY)));
	for (int y=0; y<rows; y++){
		std::vector<Piece*> row;
		for (int x=0; x<cols; x++)
			row.push_back(new Piece(Piece::EMPTY));
		board.push_back(row);
	}
	std::cout << "Creating the board\n";
}

Board::Board(std::vector<std::vector<Piece*>> &boardIn){
	rows = boardIn.size();
	cols = boardIn[0].size();
	board = boardIn;
}

bool Board::isEmpty(const std::tuple<int, int> &xy){
	return *getPiece(xy) == Piece::EMPTY;
}

Piece* Board::getPiece(const std::tuple<int, int> &xy){
	auto [x, y] = xy;
	return board[y][x];
}

void Board::set(const std::tuple<int, int> &xy, const std::tuple<int, std::string, std::string> &piece){
	auto [x, y] = xy;
	board[y][x]->setType(piece);
//	delete &piece;
}

void Board::set(const std::tuple<int, int> &xy, Piece *piece){
	auto [x, y] = xy;
	board[y][x]->setType(*piece);
//	delete piece;
}

bool Board::isValidMove(const std::tuple<int, int> &org, const std::tuple<int, int> &goal){
	return true;
}

bool Board::move(const std::tuple<int, int> &org, const std::tuple<int, int> &goal){
	// Check that there is a piece at org and no piece at goal
	if (isEmpty(org) || !isEmpty(goal))
		return false;
	if (!isValidMove(org, goal))
		return false;
	set(goal, getPiece(org));
	set(org, Piece::EMPTY);
	return true;
}

void Board::print(){
	std::string sep = "";
	for (int i=0; i<cols; i++)
		sep += "----";
	sep += "-\n";
	for (int y=0; y<rows; y++){
		std::cout << sep;
		for (int x=0; x<cols; x++)
			std::cout << "| " << *getPiece({x, y}) << " ";
		std::cout << "|\n";
	}
	std::cout << sep << "Serialized: " << serialize() << "\n";
}

std::string Board::serialize(){
	int pieces[] = {0, 0, 0, 0};
	for (int y=0; y<rows; y++)
		for (int x=0; x<cols; x++)
			if (!isEmpty({x, y}))
				pieces[getPiece({x, y})->getIndex()] |= (1u << toPos({x, y}));

	std::string serialized = "";
	for (int piece : pieces){
		std::string pieceString = std::to_string(piece);
		serialized += "|" + pieceString;
	}
	return serialized.substr(1);
}

Board Board::deserialize(const std::string &serialized){
	std::vector<std::string> pieces;
	boost::split(pieces, serialized, boost::is_any_of("|"));
	Board board;
	for (int c=0; c<pieces.size(); c++){
		int piece = std::stoi(pieces[c]);
		int pos = 0;
		while (piece != 0){
			int bit = piece & 1;
			if (bit == 1)
				board.set(fromPos(pos), Piece::typeFromIndex(c));
			pos++;
		}
	}
	return board;
}

int Board::toPos(const std::tuple<int, int> &xy, int c){
	auto [x, y] = xy;
	return y*c + x;
}

std::tuple<int, int> Board::fromPos(int pos, int c){
	return {pos%c, pos/c};
}
