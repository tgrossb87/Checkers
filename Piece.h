#ifndef PIECE_H
#define PIECE_H
#include <iostream>
#include <tuple>
#include <string>

class Piece {
	private:
		int index;
		std::string color;
		std::string token;
	public:
		const static std::tuple<int, std::string, std::string> EMPTY, GREEN, GREEN_K, RED, RED_K;

		Piece(const std::tuple<int, std::string, std::string> &type);
		bool operator== (const Piece &obj);
		bool operator== (const std::tuple<int, std::string, std::string> &type);
		bool operator!= (const Piece &obj);
		bool operator!= (const std::tuple<int, std::string, std::string> &type);
		void setType(const std::tuple<int, std::string, std::string> &type);
		void setType(Piece &obj);
		int getIndex();
		static std::tuple<int, std::string, std::string> typeFromIndex(int i);
		friend std::ostream& operator<<(std::ostream& os, const Piece &obj);
		~Piece();
};

#endif
