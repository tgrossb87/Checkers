#include "Piece.h"
#include <iostream>
#include <tuple>
#include <string>

const std::tuple<int, std::string, std::string> Piece::EMPTY = {-1, "", " "};
const std::tuple<int, std::string, std::string> Piece::GREEN = {0, "\033[42m", "O"};
const std::tuple<int, std::string, std::string> Piece::GREEN_K = {1, "\033[42m", "K"};
const std::tuple<int, std::string, std::string> Piece::RED = {2, "\033[41m", "O"};
const std::tuple<int, std::string, std::string> Piece::RED_K = {3, "\033[41m", "K"};


Piece::Piece(const std::tuple<int, std::string, std::string> &type){
	setType(type);
}

bool Piece::operator== (const Piece &obj){
	return index = obj.index;
}

bool Piece::operator== (const std::tuple<int, std::string, std::string> &type){
	auto [typeI, typeC, typeT] = type;
	return index == typeI;
}

bool Piece::operator!= (const Piece &obj){
	return index != obj.index;
}

bool Piece::operator!= (const std::tuple<int, std::string, std::string> &type){
	auto [typeI, typeC, typeT] = type;
	return index != typeI;
}

std::ostream& operator<<(std::ostream &os, const Piece &obj){
	os << obj.color << obj.token << "\033[0m";
	return os;
}

void Piece::setType(const std::tuple<int, std::string, std::string> &type){
	auto [i, c, t] = type;
	index = i;
	color = c;
	token = t;
//	delete &type;
}

void Piece::setType(Piece &obj){
	index = obj.index;
	color = obj.color;
	token = obj.token;
}

int Piece::getIndex(){
	return index;
}

std::tuple<int, std::string, std::string> Piece::typeFromIndex(int i){
	switch (i){
		case 0:
			return Piece::GREEN;
		case 1:
			return Piece::GREEN_K;
		case 2:
			return Piece::RED;
		case 3:
			return Piece::RED_K;
		default:
			return Piece::EMPTY;
	}
}

Piece::~Piece(){}
