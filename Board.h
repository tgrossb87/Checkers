#ifndef BOARD_H
#define BOARD_H
#include "Piece.h"
#include <vector>
#include <string>
#include <tuple>

class Board {
	private:
		std::vector<std::vector<Piece*>> board;
		int rows, cols;

		Board(std::vector<std::vector<Piece*>> &boardIn);

	public:
		Board(void);
		bool isEmpty(const std::tuple<int, int> &xy);
		Piece* getPiece(const std::tuple<int, int> &xy);
		bool isValidMove(const std::tuple<int, int> &org, const std::tuple<int, int> &goal);
		void set(const std::tuple<int, int> &xy, const std::tuple<int, std::string, std::string> &piece);
		void set(const std::tuple<int, int> &xy, Piece *piece);
		bool move(const std::tuple<int, int> &org, const std::tuple<int, int> &goal);

		void print();
		std::string serialize();
		static Board deserialize(const std::string &serialized);
		static int toPos(const std::tuple<int, int> &xy, int c=8);
		static std::tuple<int, int> fromPos(int pos, int c=8);
};

#endif
